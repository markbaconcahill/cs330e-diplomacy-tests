#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_eval

# --------------
# TestDiplomacy
# --------------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Mad Hold\nB Lon Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Mad', 'Hold'], ['B', 'Lon', 'Hold']])
    
    def test_read_2(self):
        s = "B Mad Hold\nA Lon Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Lon', 'Hold'], ['B', 'Mad', 'Hold']])

    def test_read_3(self):
        s = "A Mad Hold\nB Lon Hold\nC Par Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A', 'Mad', 'Hold'], ['B', 'Lon', 'Hold'], ['C', 'Par', 'Hold']])
        

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Mad Move Ric\nB Bar Move Mad\nC Lon Move Mad\nD Aus Support B\nE Par Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Ric\nB Mad\nC [dead]\nD Aus\nE Par\n")

    def test_solve_2(self):
        r = StringIO("A Mad Hold\nB Bar Move Mad\nC Lon Move Mad\nD Aus Support B\nE Par Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Aus\nE Par\n")

    def test_solve_3(self):
        r = StringIO("A Lon Move Mad\nB Mad Move Lon\nC Bos Move MaD\nD Yer Support A\nE Fre Move Mad\nF Por Support E\nG Yup Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Mad\nB Lon\nC MaD\nD Yer\nE [dead]\nF Por\nG Yup\n")

    def test_solve_4(self):
        r = StringIO("A Lon Move Mad\nB Mad Move Lon\nC Bos Move MaD\nD Yer Support A\nE Fre Move Mad\nF Por Support E\nG Yup Support A\nL Fan Move Por\nX Lam Move Ran\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Mad\nB Lon\nC MaD\nD Yer\nE [dead]\nF [dead]\nG Yup\nL [dead]\nX Ran\n")        

# ----
# main
# ----

if __name__ == "__main__":
    main()
