# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

#May need to modify Diplomacy to pass tests.

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  'A')
        self.assertEqual(j, 'Madrid')
        self.assertEqual(k, 'Hold')

    # ----
    # solve
    # ----

    #Change to Diplomacy solve

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\n')

    def test_solve_2(self):
        r = StringIO('B Madrid Hold\nC Barcelona Move Madrid\nD London Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'B [dead]\nC [dead]\nD [dead]\n')

    def test_solve_3(self):
        r = StringIO('F Madrid Hold\nG Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'F [dead]\nG [dead]\n')

    def test_solve_4(self):
        r = StringIO('I Madrid Hold\nJ London Hold\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'I Madrid\nJ London\n')

    def test_solve_5(self):
        r = StringIO('A San_Antonio Hold\nB El_Paso Move San_Antonio\n')
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print(w, 'A Madrid Hold')
        self.assertEqual(w.getvalue(), "A Madrid Hold")

# ----
# main
# ----


if __name__ == "__main__":
    main()
